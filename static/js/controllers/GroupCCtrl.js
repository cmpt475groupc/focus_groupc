/*
TEAM 1 GROUP C
CREATED BY: Justin Si, 2014-06-16
MODIFIED BY: Calvin Lii, Justin Si, Yi Ding
LAST MODIFIED: 2014-07-28

TODO:

*/
(function() {

	var INVALID_QUESTION_ERROR = 'Please ensure the question title and each answer is not empty and does not contain any "/" characters';

  this.controller('GroupCCtrl', function($scope, GroupC) {
    //console.log('GroupCCtrl');
    //$scope.hello = 'Hello from GroupCCtrl';
    GroupC.testCall().then(function(data) {
      if (data.test === "success") {
        $scope.testCall = true;
      }
    });
  });

  this.controller('questionFormCtrl', function ($scope, toaster, $http, $q, $cookies, course) {
    $scope.tab = 'addQuestion';
    $scope.questions = [];
    $scope.courseID = course.id;
    $scope.answers = [ {number: 1, content: ''}, {number: 2,content: ''} ];
    $scope.retrievedQuestions = [];
    $scope.displayedQuestions = [];
    $scope.retrievedSubmissions = [];
    
    $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken'];
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    $http.defaults.headers.put['X-CSRFToken'] = $cookies['csrftoken'];

    // Function for toast notifications
    $scope.pop = function(type, title, body, timeout, bodyOutputType, clickHandler){
            toaster.pop(type, title, body, timeout, bodyOutputType, clickHandler);
    };


	   // View questions
    $scope.v_removeQuestion = function(question) {
      var title = question.content;
      var data = question;
      //console.log('DELETE:' + data.id);
      $http.delete('/api/group_c/questions/' + data.id + '/').
        success( function () {
          $scope.pop('error', title, 'Has been removed successfully.', 3000);
          $scope.retrieveQuestions();
        }).
        error( function () {
          $scope.pop('warning', title, 'There was an error with removal.', 3000);
        });      
    };

  	$scope.editQuestion = function(question) {
      var answerSubmission = [];
      for (i=0; i < question.answers.length; i++) {
        var num = i+1;
        var ans = question.answers[i];
        var answer = {number: num, content: ans};
        answerSubmission.push(answer);
      }
      var newQuestion = { 
        id: question.id,
        course:1, 
        content:question.content, 
        activated:false, 
        answers:answerSubmission
      };
      //console.log(newQuestion);
      $scope.editableQuestion = newQuestion;
  	};

  	$scope.cancelEditQuestion = function($event) {
  		$scope.editableQuestion = null;
      $event.preventDefault();
  	};
  	
  	$scope.validQAContent = function(question) {
    if (question.content == undefined || question.content == '' || 
      question.content.indexOf("/") > -1) return false;
      for (var i=0; i<question.answers.length; i++) {
        if (question.answers[i].content == undefined || 
          question.answers[i].content == '' || 
          question.answers[i].content.indexOf("/") > -1) return false;
      };
      return true;
    }

  	$scope.submitEditQuestion = function(question) {
  		//console.log("Submitting edit");
  		if (!$scope.validQAContent(question)) {
        //console.log(question);
  			//console.log("Errors occured during edit submition");
  			$scope.pop('error', '', INVALID_QUESTION_ERROR, 3000);
  		} else {
        var newQuestion = question;
        $http.delete('/api/group_c/questions/' + question.id + '/').
        success( function () {
        }); 
  			$http.post('/api/group_c/addqa/', newQuestion).
          success( function() {
            $scope.pop('success', 'Success!', 'Questions submitted successfully!', 3000);
            $scope.retrieveQuestions();
            $scope.editableQuestion = null;
          }).
          error( function () {
            $scope.pop('error', 'Error!', 'There was a problem with the question submission', 3000);
          });
  		}
  	};
    
    /*
    $scope.saveQuestions = function($event) {
      $scope.retrievedQuestions = [];
      for (i = 0; i < $scope.displayedQuestions.length; i++) { 
        var newQuestion = angular.copy($scope.displayedQuestions[i]);
        $scope.retrievedQuestions.push(newQuestion);
      }
        $scope.alteredQuestions = false;
        $event.preventDefault();
  
    };
    
    $scope.resetQuestions = function($event) {
      $scope.displayedQuestions = [];
      for (i = 0; i < $scope.retrievedQuestions.length; i++) { 
        var newQuestion = angular.copy($scope.retrievedQuestions[i]);
        $scope.displayedQuestions.push(newQuestion);
      }
      $scope.alteredQuestions = false;
      $event.preventDefault();
    };*/
    
    
    $scope.activateQuestion = function(question) {
      var title = question.content;
    	question.activated = true;
      var data = question;
      //console.log(data)
      $http.put('/api/group_c/questions/' + data.id + '/', data).
        success( function () {
          $scope.pop('note', title, 'Has been activated', 3000);
        }).
        error( function () {
          $scope.pop('error', 'Error!', 'There was a problem activating the question', 3000);
        });
    	// api call to activate question
    };
    
    $scope.deactivateQuestion = function(question) {
      var title = question.content;
    	question.activated = false;
      var data = question;
      //console.log(data)
      $http.put('/api/group_c/questions/' + data.id + '/', data).
        success( function () {
          $scope.pop('warning', title, 'Has been deactivated', 3000);
        }).
        error( function () {
          $scope.pop('error', 'Error!', 'There was a problem deactivating the question', 3000);
        });
    	//api call to activate question
    };

    //  Add Question is used for adding questions to the list of ALL questions to be submitted.
    //  Should not be used in any backend code whatsoever. FRONT-END ONLY
    $scope.addQuestion = function() {
      var questionTitle = $scope.questionTitle;
      var answerSubmission = $scope.answers;
      var newQuestion = { 
        course:$scope.courseID, 
        content:questionTitle, 
        activated:false, 
        answers:answerSubmission
      };
      if ($scope.validQAContent(newQuestion)) {
        //console.log(newQuestion);
        $http.post('/api/group_c/addqa/', newQuestion).
          success( function() {
            $scope.pop('success', 'Success!', 'Questions submitted successfully!', 3000);
            $scope.questionTitle = '';
            $scope.answers = [ {number: 1, content: ''}, {number: 2,content: ''} ];;
          }).
          error( function () {
            $scope.pop('error', 'Error!', 'There was a problem with the question submission', 3000);
          });
      }
      else {
        $scope.pop('error', 'Error!', INVALID_QUESTION_ERROR, 3000);
      }     
    };

    // Add answer is for adding more answers to the question. FRONT-END ONLY
    $scope.addAnswer = function($event, answers) {
      var num = answers.length+1;
      answers.push( {number: num, content:''});
      $event.preventDefault();
    };

    $scope.removeAnswer = function(answers, answer) {
      var index = answers.indexOf(answer);
      if (answers.length > 2) {
        answers.splice(index, 1);
      }
      else {
        $scope.pop('error', 'Error!', 'You cannot have less than 2 answers.', 3000);     
      }
    };

    /*
    $scope.removeQuestion = function(question) {
      var index = $scope.questions.indexOf(question);
      $scope.questions.splice(index, 1);
      $scope.errors = ''
    };*/

    /*
    $scope.submitQuestions = function($event) {
      if ($scope.questions.length > 0) {
        // Backend
        var submission = $scope.questions;
        //var temp = $scope.qindex[0].qid;
        for (i = 0; i < submission.length; i++) {
          url = '/api/group_c/data/add/' + submission[i].title;
          for (j = 0; j < submission[i].answer.length; j++)
            url = url + '/' + submission[i].answer[j].ans;
          $http.get(url).success(function (data) {
            console.log(data);
            //alert(url);
          });
        }

        $scope.pop('success', 'Success!', 'Questions submitted successfully!', 3000);

        $scope.questions = [];
      }
      $event.preventDefault();
    };*/

    /* Front end presentation code, not needed
    $scope.pushAllQuestions = function(submission) {
       for (i = 0; i < submission.length; i++) { 
         $scope.retrievedQuestions.push(submission[i]);
         // Connect to the back-end
         $http.get('/api/group_c/question/add/' + questionTitle).success( function (data) {
           console.log(data);
           console.log("http request to " + '/api/group_c/question/add/' + questionTitle + " succeed");
           $scope.retrievedQuestions = data;
         });
         var newQuestion = angular.copy(submission[i]);
         $scope.displayedQuestions.push(newQuestion);
       }
       console.log($scope.retrievedQuestions);
       console.log($scope.displayedQuestions);
     };*/

    // Backend should return the same object as 'submission' or something similar.
    // This way we can parse it the exact same way in front end.

    $scope.retrieveQuestions = function() {
        $http.get('/api/group_c/questions/').
        success( function (data) {
          $scope.retrievedQuestions = data;
        }).
        error( function () {
          $scope.pop('error', 'Error!', 'There was a problem retrieving the data.', 3000);
        });
    }

    $scope.retrieveSubmissions = function() {
    	var submissions = $http.get('/api/group_c/submissions/?format=json');
    	var questions = $http.get('/api/group_c/questions/?format=json');
    	$q.all([submissions, questions]).then(function(data) { 
    			$scope.retrievedSubmissions = [];
    			var s_data = data[0].data;
    			var q_data = data[1].data;
    			//console.log(q_data);
    			var questionExists = false;
    			var existingIdx;
    			var question;
    	    	var questionIdMap = {};
    	    	var questionAnsMap = {};
    	    	var submission;
    	    	var processedSubmission;
    			for (i = 0; i < q_data.length; i++) {
    				questionIdMap[q_data[i].id] = q_data[i].content;
    				questionAnsMap[q_data[i].id] = q_data[i].answers;
    			}
    			
    			for (i = 0; i < s_data.length; i++) {
    				var retrievedSubmission = {
                			question: null,
                			answer: []
                			};
    				submission = s_data[i];
    				for (j = 0; j < $scope.retrievedSubmissions.length; j++) {
    					processedSubmission = $scope.retrievedSubmissions[j];
    					if (processedSubmission.question == questionIdMap[submission.question]) {
    						questionExists = true;
    						existingIdx = j;
    					}
    				}
    				if (questionExists){
    					retrievedSubmission = $scope.retrievedSubmissions[existingIdx];
    					retrievedSubmission.answer[submission.answer - 1].count = retrievedSubmission.answer[submission.answer - 1].count + 1;
    				}
            		else {
            			var answers = questionAnsMap[submission.question]
            			retrievedSubmission.question = questionIdMap[submission.question];
            			for (j = 0; j < answers.length; j ++) {
            				retrievedSubmission.answer.push({content : answers[j], count : 0});
            			}
            			$scope.retrievedSubmissions.push(retrievedSubmission);
            			//console.log(retrievedSubmission);
            			retrievedSubmission.answer[submission.answer - 1].count = retrievedSubmission.answer[submission.answer - 1].count + 1;
        			}
    				//console.log(retrievedSubmission);
    				
    			}
    	    })
    	}

  });

  this.directive('ngEnter', function () {
      return function (scope, element, attrs) {
          element.bind("keydown keypress", function (event) {
              if(event.which === 13) {
                  scope.$apply(function (){
                      scope.$eval(attrs.ngEnter);
                  });

                  event.preventDefault();
              }
          });
      };
  });

  this.directive('showtab',
    function () {
        return {
            link: function (scope, element, attrs) {
                element.click(function(e) {
                    e.preventDefault();
                    $(element).tab('show');
                });
            }
        };
    });

}).call(angular.module('controllers'));