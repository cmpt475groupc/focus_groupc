'''
TEAM 1 GROUP C
CREATED BY: Jack Yan, 2014-07-14
MODIFIED BY: Jack Yan, Yi Ding, Calvin Lii
LAST MODIFIED: 2014-07-25

TODO:

'''
from django.db import models
from django.contrib.auth.models import User
from accounts.models import *

import datetime

# Create your models here.

class Question(models.Model):
	course = models.ForeignKey(Course, related_name='participation')
	content = models.CharField(max_length=255, blank=False)
	creation_date = models.DateTimeField(auto_now_add=True, blank=False)
	activated = models.BooleanField(default=False)

	class Meta:
		ordering = ['creation_date']

class Answer(models.Model):
	question = models.ForeignKey(Question, related_name='answers')
	number = models.PositiveSmallIntegerField(blank=False)
	content = models.CharField(max_length=255, blank=False)

	class Meta:
		unique_together = ['question', 'number']
		ordering = ['question', 'number']

	def __unicode__(self):
		return '%s' % (self.content)

class Submission(models.Model):
	student = models.ForeignKey('auth.user')
	#student = models.PositiveIntegerField(blank=False)
	question = models.ForeignKey(Question, related_name='sub_q')
	#answer = models.ForeignKey(Answer, related_name='sub_a')
	answer = models.PositiveSmallIntegerField()
	timestamp = models.DateTimeField(auto_now=True, blank=False)

	class Meta:
		unique_together = ['student', 'question']
