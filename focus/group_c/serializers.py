'''
TEAM 1 GROUP C
CREATED BY: Jack Yan, 2014-07-22
MODIFIED BY: Jack Yan
LAST MODIFIED: 2014-07-27

TODO:

'''
from group_c.models import * 
from rest_framework import serializers

class ansSerializer(serializers.Serializer):
	number = serializers.IntegerField()
	content = serializers.CharField(max_length=255)

class quesSerializer(serializers.Serializer):
	course = serializers.IntegerField()
	content = serializers.CharField(max_length=255)
	activated = serializers.BooleanField()
	answers = ansSerializer(many=True)

class AnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Answer
		fields = ('question', 'number', 'content')

class QuestionSerializer(serializers.ModelSerializer):
	answers = serializers.RelatedField(many=True)

	class Meta:
		model = Question
		fields = ('id', 'course', 'content', 'creation_date', 'activated', 'answers')

class SubmissionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Submission
		fields = ('student', 'question', 'answer', 'timestamp')
