'''
TEAM 1 GROUP C
CREATED BY: Jack Yan, 2014-07-22
MODIFIED BY: Yi Ding, Jack Yan
LAST MODIFIED: 2014-07-27

TODO:

'''
from django.shortcuts import render

from django.views.generic import View
from django.core import serializers
from django.http import HttpResponse
from django.http import Http404

from rest_framework import filters, generics, status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

from accounts.models import *
from group_c.models import * 
from group_c.serializers import * 

class AddQuestionAnswer(View):
	def post(self, request):
		data = JSONParser().parse(request)
		s = quesSerializer(data=data)
		if s.is_valid():
			answers = s.data['answers']
			s_q = QuestionSerializer(data=s.data)
			if s_q.is_valid():
				q = s_q.save()
				for answer in answers:
					answer['question'] = q.id
					ans = AnswerSerializer(data=answer)
					if ans.is_valid():
						ans.save()
					else:
						return HttpResponse(ans.errors, status=status.HTTP_400_BAD_REQUEST)
			else:
				return HttpResponse(s_q.errors, status=status.HTTP_400_BAD_REQUEST)
		else:
			return HttpResponse(s.errors, status=status.HTTP_400_BAD_REQUEST)
		return HttpResponse(status=status.HTTP_201_CREATED)

class QuestionViewSet(generics.ListCreateAPIView):
	queryset = Question.objects.all()
	serializer_class = QuestionSerializer
	filter_fields = ['id', 'course', 'content', 'creation_date']

class QuestionActiveViewSet(generics.ListAPIView):
	queryset = Question.objects.filter(activated=True)
	serializer_class = QuestionSerializer
	filter_fields = ['id', 'course', 'content', 'creation_date']

class QuestionDetailView(generics.RetrieveUpdateDestroyAPIView):
	queryset = Question.objects.all()
	serializer_class = QuestionSerializer

class AnswerViewSet(generics.ListCreateAPIView):
	queryset = Answer.objects.all()
	serializer_class = AnswerSerializer

class SubmissionViewSet(generics.ListCreateAPIView):
	queryset = Submission.objects.all()
	serializer_class = SubmissionSerializer
	filter_fields = ['student', 'question', 'answer', 'timestamp']
