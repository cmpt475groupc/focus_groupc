"""Account management tests.

tests.py
Worked on by: Tom Dryer (Team 1, Group A)
"""

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase

from accounts.models import Course, get_instructor_group


class LoginTests(APITestCase):

    def setUp(self):
        User.objects.create_user('tom@example.com', 'tom@example.com', 'test')

    def test_successful_login(self):
        response = self.client.post('/api/accounts/session', {
            'email': 'tom@example.com',
            'password': 'test'
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for ignored in ['sessionid', 'csrftoken']:
            if ignored in response.data:
                response.data[ignored] = None
        self.assertEqual(response.data, {
            'email': 'tom@example.com',
            'id': 2,
            'is_admin': False,
            'is_instructor': False,
            'name': '',
            'sessionid': None,
            'csrftoken': None,
        })
        self.assertEqual(set(response.cookies.keys()),
                         set(['sessionid', 'csrftoken']))

    def test_invalid_credentials(self):
        response = self.client.post('/api/accounts/session', {
            'email': 'tom@example.com',
            'password': 'invalid'
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'Incorrect authentication credentials.'
        })
        self.assertEqual(list(response.cookies.keys()), [])

    def test_invalid_request(self):
        response = self.client.post('/api/accounts/session', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'detail': {
                'email': ['This field is required.'],
                'password': ['This field is required.'],
            }
        })
        self.assertEqual(list(response.cookies.keys()), [])


class GetSessionTests(APITestCase):

    def setUp(self):
        User.objects.create_user('tom@example.com', 'tom@example.com', 'test',
                                 first_name='Tom Dryer')

    def test_unauthorized(self):
        response = self.client.get('/api/accounts/session', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_authorized_not_instructor(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.get('/api/accounts/session', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'email': 'tom@example.com',
            'is_instructor': False,
            'name': 'Tom Dryer',
            'is_admin': False,
            'id': 2,
        })

    def test_authorized_is_instructor(self):
        self.client.login(username='admin@example.com', password='admin')
        response = self.client.get('/api/accounts/session', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'email': 'admin@example.com',
            'is_instructor': True,
            'name': '',
            'is_admin': True,
            'id': 1,
        })


class LogoutTests(APITestCase):

    def setUp(self):
        User.objects.create_user('tom@example.com', 'tom@example.com', 'test')

    def test_unauthorized(self):
        response = self.client.delete('/api/accounts/session', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_logout(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.delete('/api/accounts/session', format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.delete('/api/accounts/session', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ChangeSessionTests(APITestCase):

    def setUp(self):
        User.objects.create_user('tom@example.com', 'tom@example.com', 'test')

    def test_unauthorized(self):
        response = self.client.patch('/api/accounts/session', {},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_bad_request(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.patch('/api/accounts/session', {'foo': 'bar'},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_change_password(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.patch('/api/accounts/session', {
            'old_password': 'test',
            'new_password': 'foobarbaz'
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # Try logging in with the new password to check it was set.
        self.client.logout()
        self.client.login(username='tom@example.com', password='foobarbaz')
        response = self.client.patch('/api/accounts/session', {},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_incorrect_old_password(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.patch('/api/accounts/session', {
            'old_password': 'incorrect',
            'new_password': 'foobar'
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'detail': 'Old password does not match.'})

    def test_unacceptable_new_password(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.patch('/api/accounts/session', {
            'old_password': 'test',
            'new_password': 'poop'
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {'detail': 'New password is too short.'})

    def test_change_name(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.patch('/api/accounts/session', {
            'name': 'Tom Dryer',
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        user = User.objects.filter(email='tom@example.com').first()
        self.assertEqual(user.first_name, 'Tom Dryer')

    def test_change_name_blank(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.patch('/api/accounts/session', {
            'name': '',
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        user = User.objects.filter(email='tom@example.com').first()
        self.assertEqual(user.first_name, '')

    def test_change_name_too_long(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.patch('/api/accounts/session', {
            'name': 'a' * 300,
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ViewInstructorsTests(APITestCase):

    def setUp(self):
        instructor1 = User.objects.create_user('instructor1@example.com',
                                               'instructor1@example.com',
                                               'test',
                                               first_name='Instructor 1')
        instructor2 = User.objects.create_user('instructor2@example.com',
                                               'instructor2@example.com',
                                               'test')
        get_instructor_group().user_set.add(instructor1)
        get_instructor_group().user_set.add(instructor2)

    def test_requires_login(self):
        response = self.client.get('/api/accounts/instructors', {},
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'Authentication credentials were not provided.',
        })

    def test_requires_admin(self):
        self.client.login(username='instructor1@example.com', password='test')
        response = self.client.get('/api/accounts/instructors', {},
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.',
        })

    def test_success(self):
        self.client.login(username='admin@example.com', password='admin')
        response = self.client.get('/api/accounts/instructors', {},
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'instructors': [
                {
                    'email': 'admin@example.com',
                    'name': '',
                    'id': 1,
                },
                {
                    'email': 'instructor1@example.com',
                    'name': 'Instructor 1',
                    'id': 2,
                },
                {
                    'email': 'instructor2@example.com',
                    'name': '',
                    'id': 3,
                },
            ]
        })


class CreateInstructorTests(APITestCase):

    def setUp(self):
        User.objects.create_user('tom@example.com', 'tom@example.com', 'test')
        # Also using the admin created by account fixtures.

    def test_invalid_request(self):
        self.client.login(username='admin@example.com', password='admin')
        response = self.client.post('/api/accounts/instructors', {},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'detail': {
                'email': ['This field is required.'],
            }
        })

    def test_requires_login(self):
        response = self.client.post('/api/accounts/instructors', {},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'Authentication credentials were not provided.',
        })

    def test_requires_admin(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.post('/api/accounts/instructors', {},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.',
        })

    def test_duplicate_email(self):
        self.client.login(username='admin@example.com', password='admin')
        response = self.client.post(
            '/api/accounts/instructors', {'email': 'tom@example.com'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'detail': 'Instructor with this email already exists.',
        })

    def test_success(self):
        self.client.login(username='admin@example.com', password='admin')
        response = self.client.post(
            '/api/accounts/instructors', {'email': 'david@example.com'},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        user = User.objects.filter(email='david@example.com').first()
        self.assertIsNotNone(user)
        self.assertIn(get_instructor_group(), user.groups.all())


class CreateCourseTests(APITestCase):

    def setUp(self):
        instructor = User.objects.create_user('instructor@example.com',
                                              'instructor@example.com', 'test')
        get_instructor_group().user_set.add(instructor)
        User.objects.create_user('tom@example.com', 'tom@example.com', 'test')
        Course.objects.create(name='CMPT 475 E100 Summer 2014',
                              instructor=instructor)

    def test_invalid_request(self):
        self.client.login(username='instructor@example.com', password='test')
        response = self.client.post('/api/accounts/courses', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'detail': {
                'name': ['This field is required.'],
                'student_emails': ['This field is required.'],
            }
        })

    def test_requires_login(self):
        response = self.client.post('/api/accounts/courses', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'Authentication credentials were not provided.',
        })

    def test_requires_instructor(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.post('/api/accounts/courses', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.',
        })

    def test_duplicate_name(self):
        self.client.login(username='instructor@example.com', password='test')
        response = self.client.post('/api/accounts/courses', {
            'name': 'CMPT 475 E100 Summer 2014',
            'student_emails': ['david@example.com'],
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {
            'detail': 'Course with this name already exists.'
        })

    def test_success(self):
        self.client.login(username='instructor@example.com', password='test')
        response = self.client.post('/api/accounts/courses', {
            'name': 'IAT 110 D100 Summer 2014',
            'student_emails': ['david@example.com', 'tom@example.com'],
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        course = Course.objects.filter(name='IAT 110 D100 Summer 2014').first()
        self.assertIsNotNone(course)
        self.assertEqual(course.instructor.email, 'instructor@example.com')
        students = sorted(u.email for u in course.enrolled_users.all())
        # david should have his account be created and added to the course
        self.assertEqual(students, ['david@example.com', 'tom@example.com'])


class ViewCoursesTests(APITestCase):

    def setUp(self):
        instructor1 = User.objects.create_user('instructor1@example.com',
                                               'instructor1@1example.com',
                                               'test')
        get_instructor_group().user_set.add(instructor1)
        course1 = Course.objects.create(name='Course 1', instructor=instructor1)

        instructor2 = User.objects.create_user('instructor2@example.com',
                                               'instructor2@1example.com',
                                               'test')
        get_instructor_group().user_set.add(instructor2)
        course2 = Course.objects.create(name='Course 2', instructor=instructor2)
        student1 = User.objects.create_user('tom@example.com',
                                            'tom@example.com', 'test')
        course1.enrolled_users.add(student1)
        student2 = User.objects.create_user('david@example.com',
                                            'david@example.com', 'test')
        course1.enrolled_users.add(student2)

    def test_requires_login(self):
        response = self.client.get('/api/accounts/courses', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'Authentication credentials were not provided.',
        })

    def test_instructor(self):
        self.client.login(username='instructor1@example.com', password='test')
        response = self.client.get('/api/accounts/courses', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [
            {'name': 'Course 1', 'id': 1,
             'instructor': 'instructor1@example.com',
             'student_emails': ['tom@example.com', 'david@example.com']},
        ])

    def test_student(self):
        self.client.login(username='tom@example.com', password='test')
        response = self.client.get('/api/accounts/courses', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [
            {'name': 'Course 1', 'id': 1,
             'instructor': 'instructor1@example.com',
             'student_emails': ['tom@example.com', 'david@example.com']},
        ])


class ModifyCourseTests(APITestCase):

    def setUp(self):
        instructor1 = User.objects.create_user('instructor1@example.com',
                                               'instructor1@1example.com',
                                               'test')
        get_instructor_group().user_set.add(instructor1)
        self.course1 = Course.objects.create(name='Course 1', instructor=instructor1)

        instructor2 = User.objects.create_user('instructor2@example.com',
                                               'instructor2@1example.com',
                                               'test')
        get_instructor_group().user_set.add(instructor2)
        self.student1 = User.objects.create_user('tom@example.com',
                                                 'tom@example.com', 'test')
        self.course1.enrolled_users.add(self.student1)
        self.student2 = User.objects.create_user('david@example.com',
                                                 'david@example.com', 'test')

    def test_not_found(self):
        self.client.login(username='instructor1@example.com', password='test')
        response = self.client.patch('/api/accounts/courses/2', {},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_requires_login(self):
        response = self.client.patch('/api/accounts/courses/1', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'Authentication credentials were not provided.',
        })

    def test_wrong_instructor(self):
        self.client.login(username='instructor2@example.com', password='test')
        response = self.client.patch('/api/accounts/courses/1', {},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.',
        })

    def test_modify_students(self):
        self.client.login(username='instructor1@example.com', password='test')
        # remove tom and add david
        response = self.client.patch('/api/accounts/courses/1', {
            'student_emails': ['david@example.com']
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertNotIn(self.student1, self.course1.enrolled_users.all())
        self.assertIn(self.student2, self.course1.enrolled_users.all())


class ViewCourseTests(APITestCase):

    def setUp(self):
        instructor1 = User.objects.create_user('instructor1@example.com',
                                               'instructor1@1example.com',
                                               'test')
        get_instructor_group().user_set.add(instructor1)
        self.course1 = Course.objects.create(name='Course 1', instructor=instructor1)

        instructor2 = User.objects.create_user('instructor2@example.com',
                                               'instructor2@1example.com',
                                               'test')
        get_instructor_group().user_set.add(instructor2)
        self.student1 = User.objects.create_user('tom@example.com',
                                                 'tom@example.com', 'test')
        self.course1.enrolled_users.add(self.student1)
        self.student2 = User.objects.create_user('david@example.com',
                                                 'david@example.com', 'test')

    def test_not_found(self):
        self.client.login(username='instructor1@example.com', password='test')
        response = self.client.get('/api/accounts/courses/2', format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_requires_login(self):
        response = self.client.get('/api/accounts/courses/1', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'Authentication credentials were not provided.',
        })

    def test_not_student(self):
        self.client.login(username='david@example.com', password='test')
        response = self.client.get('/api/accounts/courses/1', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.',
        })

    def test_not_instructor(self):
        self.client.login(username='instructor2@example.com', password='test')
        response = self.client.get('/api/accounts/courses/1', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data, {
            'detail': 'You do not have permission to perform this action.',
        })

    def test_student(self):
        self.client.login(username='tom@example.com', password='test')
        # remove tom and add david
        response = self.client.get('/api/accounts/courses/1', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'id': 1, 'name': 'Course 1',
            'instructor': 'instructor1@example.com',
            'student_emails': ['tom@example.com'],
        })

    def test_instructor(self):
        self.client.login(username='instructor1@example.com', password='test')
        # remove tom and add david
        response = self.client.get('/api/accounts/courses/1', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {
            'id': 1, 'name': 'Course 1',
            'instructor': 'instructor1@example.com',
            'student_emails': ['tom@example.com'],
        })
