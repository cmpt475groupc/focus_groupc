worker_processes 1;

events {

    worker_connections 1024;

}

http {

    sendfile on;

    gzip              on;
    gzip_http_version 1.0;
    gzip_proxied      any;
    gzip_min_length   500;
    gzip_disable      "MSIE [1-6]\.";
    gzip_types        text/plain text/xml text/css
                      text/comma-separated-values
                      text/javascript
                      application/x-javascript
                      application/atom+xml;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    # Configuration containing list of application servers
    upstream uwsgicluster {

        server 127.0.0.1:8080;
        # server 127.0.0.1:8081;
        # ..
        # .

    }

    # Configuration for Nginx
    server {

        # Running port
        listen 80;

        # Settings to by-pass for static files
        location ^~ /static/  {
            root /focus;
        }

        # Settings to by-pass for media files (file management)
        location ^~ /media/  {
            root /focus;
        }

        # Support AngularJS HTML5 mode routing
        # TODO: this is not ideal, it creates alterate routes for everything in /static
        location / {
            root /focus/static;
            try_files $uri $uri/ /index.html =404;
        }

        # Proxying connections to application servers
        location /api {

            uwsgi_param     QUERY_STRING            $query_string;
            uwsgi_param     REQUEST_METHOD          $request_method;
            uwsgi_param     CONTENT_TYPE            $content_type;
            uwsgi_param     CONTENT_LENGTH          $content_length;

            uwsgi_param     REQUEST_URI             $request_uri;
            uwsgi_param     PATH_INFO               $document_uri;
            uwsgi_param     DOCUMENT_ROOT           $document_root;
            uwsgi_param     SERVER_PROTOCOL         $server_protocol;
            uwsgi_param     UWSGI_SCHEME            $scheme;

            uwsgi_param     REMOTE_ADDR             $remote_addr;
            uwsgi_param     REMOTE_PORT             $remote_port;
            uwsgi_param     SERVER_PORT             $server_port;
            uwsgi_param     SERVER_NAME             $server_name;

            uwsgi_pass         uwsgicluster;

            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;

        }
    }
}
